package UsingForm;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
public class AddDetails {
	public AddDetails() {
		JFrame frame=new JFrame(" This is Add Operation Frame Page ");
		frame.setSize(600, 400); 
        
		JLabel Id=new JLabel(" Enter The ID		 : ");
		Id.setBounds(100,50,200,40);
		frame.add(Id);
		
		JLabel Name=new JLabel(" Enter The Name    : ");
		Name.setBounds(100,100,200,40);
		frame.add(Name);
		
		JLabel Country=new JLabel(" Enter The Country : ");
		Country.setBounds(100,150,200,40);
		frame.add(Country);
		
		JLabel Salary=new JLabel(" Enter The Salary  : ");
		Salary.setBounds(100,200,200,40);
		frame.add(Salary);
		
		JTextField IdField= new JTextField ();
		IdField.setBounds(270,60,100,25);
		frame.add(IdField);
		
		JTextField NameField= new JTextField ();
		NameField.setBounds(270,110,100,25);
		frame.add(NameField);
		
		JTextField CountryField= new JTextField ();
		CountryField.setBounds(270,160,100,25);
		frame.add(CountryField);
		
		JTextField SalaryField= new JTextField ();
		SalaryField.setBounds(270,210,100,25);
		frame.add(SalaryField);
		
		
		JButton addButton=new JButton(" ADD");
		addButton.setBounds(200,300,100,25);
		frame.add(addButton);
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				String UserId=IdField.getText(); 
				String UserName=NameField.getText();
				String UserCountry=CountryField.getText(); 
				String UserSalary=SalaryField.getText();
				Connection con = null; 
				String url = "jdbc:oracle:thin:@localhost:1521:xe";
				String driver = "oracle.jdbc.driver.OracleDriver"; 
				String username = "system"; 
				String password = "12345"; 
				try{					 
				Class.forName(driver); 
				con = DriverManager.getConnection(url, username, password);  
				System.out.println("hi before");
				PreparedStatement stmt=con.prepareStatement("insert into Cricketers values(?,?,?,?)");  
				stmt.setString(1,UserId);
				stmt.setString(2,UserName);  
				stmt.setString(3,UserCountry); 
				stmt.setString(4, UserSalary);
				int i=stmt.executeUpdate();  
				System.out.println(i+" record inserted");  
				System.out.println("hi after");
				}
				catch(Exception ex1){  
				System.out.println(ex1.getMessage()); 
				} 	
			}
			});
		frame.setLayout(null);
		frame.setVisible(true);
	}
}
