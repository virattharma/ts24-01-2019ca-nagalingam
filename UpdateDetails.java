package UsingForm;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
public class UpdateDetails {
	public UpdateDetails() {
		 JFrame frame=new JFrame(" This is Update Details Frame Page");
		 frame.setSize(400,500);
		 
		 JLabel ID=new JLabel("ID");
		 ID.setBounds(23,45,100,30);
		 frame.add(ID);
		  
		 JTextField IDField=new JTextField();
		 IDField.setBounds(200,45,100,30);
		 frame.add(IDField);
		 
		 JLabel name=new JLabel("Name");
		 name.setBounds(23,100,150,30);
		 frame.add(name);
		 
		 JTextField nameField=new JTextField();
		 nameField.setBounds(200,100,100,30);
		 frame.add(nameField);
		  
		 JLabel Country=new JLabel("Enter the Country name");
		 Country.setBounds(23,150,200,30);
		 frame.add(Country);
		 
		 JTextField CountryField=new JTextField();
		 CountryField.setBounds(200,150,100,30);
		 frame.add(CountryField);
		 
		 JLabel Salary=new JLabel(" Salary ");
		 Salary.setBounds(23,200,250,30);
		 frame.add(Salary);
		 
		 JTextField SalaryField=new JTextField();
		 SalaryField.setBounds(200,200,100,30);
		 frame.add(SalaryField);
		  
		 JButton Update=new JButton("Update");
		 Update.setBounds(150,280,100,30);
		 frame.add(Update);
	 	 Update.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 try{  
					 Class.forName("oracle.jdbc.driver.OracleDriver");  
					 Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","12345");  
					 String name=nameField.getText();
					 int Id=Integer.parseInt(IDField.getText());
					 String Country=CountryField.getText();
					 float Salary=Float.parseFloat(SalaryField.getText());
					 PreparedStatement stmt=con.prepareStatement("update Cricketers set name='"+name+"',Country='"+Country+"',Salary='"+Salary+"' where id="+Id);
					 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+"record Updated"); 
					 System.out.println(i+" record Updated");    
					 con.close();  
					 }
				 catch(Exception e1){ 
						 System.out.println(e1);
						 }  
					 }
		        } );
	 frame.setLayout(null);
	 frame.setVisible(true);
	}
	}
