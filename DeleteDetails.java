package UsingForm;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
public class DeleteDetails {
	public DeleteDetails() {
		JFrame frame=new JFrame(" This is Delete Operation Frame Page ");
		frame.setSize(400, 300); 
        
		JLabel Id=new JLabel(" Enter The ID	 : ");
		Id.setBounds(100,50,200,40);
		frame.add(Id);
		
		JTextField IdField= new JTextField ();
		IdField.setBounds(200,60,100,25);
		frame.add(IdField);
		
		JButton deleteButton=new JButton(" Delete ");
		deleteButton.setBounds(150,120,100,25);
		frame.add(deleteButton);
		deleteButton.addActionListener(new ActionListener(){  	 
			 public void actionPerformed(ActionEvent e){  
				 try{  
					 Class.forName("oracle.jdbc.driver.OracleDriver");  
					 Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","12345");  
					 PreparedStatement stmt=con.prepareStatement("delete from Cricketers where id=(?)");  
					 stmt.setString(1,IdField.getText());
				  	 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+" record deleted"); 
					 System.out.println(i+" record deleted");    
					 con.close();  
					 }
				 catch(Exception e1){ 
						 System.out.println(e1);
						 }
				}
		        }  );
		frame.setLayout(null);
		frame.setVisible(true);
		}
}
