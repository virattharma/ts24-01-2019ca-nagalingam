package UsingForm;
import java.awt.event.*;
import javax.swing.*;
public class MainFrame {
	public static void main(String[] args) {
		JFrame Mainframe=new JFrame(" This is MainFrame Page ");
		Mainframe.setSize(450, 550); 
		
		JButton add= new JButton(" Add ");
		add.setBounds(150,120,100,40);
		Mainframe.add(add);
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AddDetails();
				Mainframe.dispose();
			}
			});
		
		JButton update= new JButton(" Update ");
		update.setBounds(150,220,100,40);
		Mainframe.add(update);
		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateDetails ud=new UpdateDetails();
				Mainframe.dispose();
			}
			});
		
		JButton delete= new JButton(" Delete ");
		delete.setBounds(150,320,100,40);
		Mainframe.add(delete);
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DeleteDetails dd=new DeleteDetails();
				Mainframe.dispose();
			}
			});
		Mainframe.setLayout(null);
		Mainframe.setVisible(true);
	}
}
